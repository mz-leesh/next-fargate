export const MAILING_INIT = "MAILING_INIT";
export const MAILING_REQUEST = "MAILING_REQUEST";

// Action creators
export const mailingInit = () => {
  return {
    type: MAILING_INIT,
  };
};
export const mailing = (data) => {
  return {
    type: MAILING_REQUEST,
    data,
  };
};
