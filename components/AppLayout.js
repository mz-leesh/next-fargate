import React, { Component } from "react";
import Link from "next/link";
import PropTypes from "prop-types";

class AppLayout extends Component {
  render() {
    const { children } = this.props;
    return (
      <div>
        {/*<div*/}
        {/*  style={{*/}
        {/*    backgroundColor: "#000",*/}
        {/*    height: 30,*/}
        {/*    lineHeight: "30px",*/}
        {/*  }}*/}
        {/*>*/}
        {/*  <ul style={{ display: "inline" }}>*/}
        {/*    <li style={{ display: "inline-block", paddingLeft: 20 }}>*/}
        {/*      <Link href="/">*/}
        {/*        <a>홈</a>*/}
        {/*      </Link>*/}
        {/*    </li>*/}
        {/*    <li style={{ display: "inline-block", paddingLeft: 20 }}>*/}
        {/*      <Link href="/cafe">*/}
        {/*        <a>카페</a>*/}
        {/*      </Link>*/}
        {/*    </li>*/}
        {/*    <li style={{ display: "inline-block", paddingLeft: 20 }}>*/}
        {/*      <Link href="/review">*/}
        {/*        <a>리뷰</a>*/}
        {/*      </Link>*/}
        {/*    </li>*/}
        {/*    <li style={{ display: "inline-block", paddingLeft: 20 }}>*/}
        {/*      <Link href="/my">*/}
        {/*        <a>찜</a>*/}
        {/*      </Link>*/}
        {/*    </li>*/}
        {/*  </ul>*/}
        {/*</div>*/}
        <div>{children}</div>
      </div>
    );
  }
}

AppLayout.propTypes = {
  children: PropTypes.node,
};

export default AppLayout;
