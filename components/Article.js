import React, { Component } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import { Menu, Input, Button, Row, Col, Card, Avatar } from "antd";

class Article extends Component {
  render() {
    return (
      <li style={{ display: "inline-block", marginRight: 10 }}>
        <div style={{ width: 270 }}>
          <div style={{ width: 270, height: 200, backgroundColor: "green" }} />
          <div style={{ marginTop: 5, fontWeight: "bold", fontSize: 15 }}>
            제주 이너프
          </div>
          <div style={{ marginTop: 5, color: "#AAA" }}>
            앞산에서 제일 좋아했던 엔틱한 로우펜스
            카페, 무엇보다 주택개조 카페라 정말 예뻤는데 말이죠!
          </div>
          <div style={{ marginTop: 5 }}>
            제주 제주시 애월읍 고성8길 33
          </div>
        </div>
      </li>
    );
  }
}

export default Article;
