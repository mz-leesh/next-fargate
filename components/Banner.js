import React, { Component } from "react";
import Link from "next/link";
import PropTypes from "prop-types";
import { Menu, Input, Button, Row, Col, Card, Avatar } from "antd";

class Banner extends Component {
  render() {
    return (
      <div
        style={{
          position: "relative",
          backgroundColor: "red",
          width: "100%",
          height: 500,
        }}
      >
        <div style={{ width: 600, height: 400, padding: 30 }}>
          <img
            src="https://scontent-ssn1-1.cdninstagram.com/v/t51.2885-15/sh0.08/e35/s640x640/131403156_235541651241493_2857539164725451673_n.jpg?_nc_ht=scontent-ssn1-1.cdninstagram.com&_nc_cat=101&_nc_ohc=FBD-xXhdIkkAX8ay64M&tp=1&oh=d8f06a8240428ca9f0ef9e49d1714f25&oe=6000B3D0"
            width="600"
            height="400"
          />
        </div>
        <div
          style={{
            position: "absolute",
            left: 630,
            top: 30,
            backgroundColor: "blue",
            fontSize: 26,
          }}
        >
          <div>세상의 모든 카페, 모카</div>
        </div>
      </div>
    );
  }
}

Banner.propTypes = {
  children: PropTypes.node,
};

export default Banner;
