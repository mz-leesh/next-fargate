import React from "react";
import Head from "next/head";
import PropTypes from "prop-types";
import withRedux from "next-redux-wrapper";
import withReduxSaga from "next-redux-saga";
import { applyMiddleware, compose, createStore } from "redux";
import { Provider } from "react-redux";
import createSagaMiddleware from "redux-saga";

import AppLayout from "../components/AppLayout";
import reducer from "../reducers";
import rootSaga from "../sagas";
import { LOAD_USER_REQUEST } from "../reducers/user";

const Moca = ({ Component, store }) => {
  return (
    <Provider store={store}>
      <AppLayout>
        <Component />
      </AppLayout>
    </Provider>
  );
};

Moca.propTypes = {
  Component: PropTypes.elementType.isRequired,
  store: PropTypes.object.isRequired,
};

Moca.getInitialProps = async (context) => {
  const { ctx, Component } = context;
  let pageProps = {};
  const state = ctx.store.getState();

  // if (!state.me) {
  //   if (typeof localStorage !== "undefined") {
  //     const accessToken = localStorage.getItem("accessToken");
  //     console.log("accessToken1:", accessToken);
  //     ctx.store.dispatch({
  //       type: LOAD_USER_REQUEST,
  //       data: {
  //         accessToken: accessToken,
  //       },
  //     });
  //   }
  // }

  if (Component.getInitialProps) {
    pageProps = (await Component.getInitialProps(ctx, state)) || {};
  }

  return { pageProps };
};

const configureStore = (initialState, options) => {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [sagaMiddleware];
  const enhancer =
    process.env.NODE_ENV === "production"
      ? compose(applyMiddleware(...middlewares))
      : compose(
          applyMiddleware(...middlewares),
          !options.isServer &&
            typeof window.__REDUX_DEVTOOLS_EXTENSION__ !== "undefined"
            ? window.__REDUX_DEVTOOLS_EXTENSION__()
            : (f) => f
        );
  const store = createStore(reducer, initialState, enhancer);
  store.sagaTask = sagaMiddleware.run(rootSaga);
  return store;
};

export default withRedux(configureStore)(withReduxSaga(Moca));
