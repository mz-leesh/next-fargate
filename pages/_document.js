import Document, { Html, Head, Main, NextScript } from "next/document";
import React from "react";

// 기본 문서 형식 지정
// 서버사이드에서 단 1회만 로드
// SPA에서 변경할 수 없는 부분
// 클라이언트단의 첫 진입점 _app.js
export default class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
            <title>모카, 세상의 모든 카페</title>
            <meta name="description" content="세상의 모든 카페 리뷰를 남겨요, 세상의 모든 카페 추천"/>
            <meta property="og:url" content="https://www.moca.so/"/>
            <meta property="og:title" content="모카, 세상의 모든 카페"/>
            <meta property="og:description" content="세상의 모든 카페 리뷰를 남겨요, 세상의 모든 카페 추천"/>
            <meta property="og:site_name" content="모카"/>
            <meta property="og:image" content="https://cdn.moca.so/images/meta/moca.png"/>
            <meta property="og:type" content="article"/>
            <meta property="og:locale" content="ko_KR"/>
            <meta property="fb:app_id" content=""/>
            <meta name="twitter:card" content="summary_large_image"/>
            <meta name="twitter:site" content=""/>
            <meta name="twitter:title" content="모카, 세상의 모든 카페"/>
            <meta name="twitter:description" content="세상의 모든 카페 리뷰를 남겨요, 세상의 모든 카페 추천"/>
            <meta name="twitter:image" content="https://cdn.moca.so/images/meta/moca.png"/>
            <meta name="author" content="모카"/>
            <meta name="naver-site-verification" content=""/>
        </Head>
        <body style={{ margin: 0, padding: 0 }}>
          <div id="root">
            <Main />
            <NextScript />
          </div>
        </body>
      </Html>
    );
  }
}
