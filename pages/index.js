import React, { Component } from "react";
// import Image from "next/image";
import styled from "styled-components";
import { connect } from "react-redux";
import * as actions from "../actions/main";

const Input = styled.input`
  padding: 0 25px;
  ::placeholder,
  ::-webkit-input-placeholder {
    color: #b4b4b4;
    font-size: 18px;
  }
  :-ms-input-placeholder {
    color: #b4b4b4;
    font-size: 18px;
  }
`;

class Home extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: "",
    };
  }

  componentDidMount() {
    const UserAgent = navigator.userAgent;
    if (
      UserAgent.match(
        /iPhone|ipad|Android|Windows CE|BlackBerry|Symbian|Windows Phone|webOS|Opera Mini|Opera Mobi|POLARIS|IEMobile|lgtelecom|nokia|SonyEricsson/i
      ) != null ||
      UserAgent.match(/LG|SAMSUNG|Samsung/) != null
    ) {
      location.href = "https://m.moca.so";
    }
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (
      prevProps.resultYN !== this.props.resultYN &&
      this.props.resultYN === "Y"
    ) {
      alert(this.props.message);
      this.props.mailingInit();
      this.setState({
        email: "",
      });
    }
  }

  checkEmail = (str) => {
    var reg_email = /^([0-9a-zA-Z_\.-]+)@([0-9a-zA-Z_-]+)(\.[0-9a-zA-Z_-]+){1,2}$/;
    if (!reg_email.test(str)) {
      return false;
    } else {
      return true;
    }
  };

  onChangeEmail = (e) => {
    this.setState({
      email: e.target.value,
    });
  };

  onMailing = () => {
    if (this.state.email) {
      if (!this.checkEmail(this.state.email)) {
        alert("이메일 주소 형식이 올바르지 않습니다.");
        return false;
      } else {
        this.props.mailing({
          email: this.state.email,
          type: "pc",
        });
      }
    } else {
      alert("이메일 주소를 입력해주세요.");
      document.getElementById("email").focus();
      return false;
    }
  };

  render() {
    return (
      <>
        <div
          style={{
            width: "100%",
            height: 839,
            background: `url('/bgImg2.png')`,
            textAlign: "center",
            paddingTop: 242,
          }}
        >
          <img src="/logo.png" width="191" height="258" />
          <div
            style={{
              marginTop: 16,
              color: "#FFF",
              fontSize: 22,
              fontFamily: "NotoSansCJKkr-Medium",
              letterSpacing: -0.53,
            }}
          >
            모카는 세상의 모든 카페, 고객님의 취향에 맞는 카페를 추천해주며
            <br />
            SNS인기카페, 포토존, 핫플레이스의 카페들을 안내해드립니다.(배포 Test)
          </div>
          <div
            style={{
              marginTop: 40,
              width: 610,
              position: "relative",
              left: "50%",
              marginLeft: -305,
            }}
          >
            <div
              style={{
                float: "left",
              }}
            >
              <Input
                type="text"
                id="email"
                style={{
                  width: 450,
                  height: 72,
                  borderTopLeftRadius: 12,
                  borderBottomLeftRadius: 12,
                  border: 0,
                  fontSize: 18,
                }}
                placeholder="메일주소를 보내주시면 향후 안내 메일링을 보내드립니다."
                autoComplete="off"
                onChange={this.onChangeEmail}
                value={this.state.email}
              />
            </div>
            <div style={{ float: "right" }}>
              <div
                style={{
                  width: 110,
                  height: 72,
                  borderTopRightRadius: 12,
                  borderBottomRightRadius: 12,
                  backgroundColor: "#ec7a56",
                  color: "#FFF",
                  fontSize: 18,
                  letterSpacing: -0.43,
                  textAlign: "center",
                  lineHeight: "72px",
                }}
                onClick={this.onMailing}
              >
                보내기
              </div>
            </div>
          </div>
          <div
            style={{
              paddingTop: 50,
              width: "100%",
              display: "flex",
              justifyContent: "center",
            }}
          >
            <div
              style={{
                display: "flex",
                alignItems: "center",
              }}
            >
              <div style={{ flex: 1, alignItems: "flex-start" }}>
                <a
                  href="https://instagram.com/moca.so_official"
                  target="_blank"
                >
                  <img src="/instagramLogo.png" width="40" height="40" />
                </a>
              </div>
              <div style={{ marginLeft: 5, fontSize: 22 }}>
                <a
                  href="https://instagram.com/moca.so_official"
                  target="_blank"
                  style={{ color: "#FFF", textDecoration: "none" }}
                >
                  @moca.so_official
                </a>
              </div>
            </div>
          </div>
          <div style={{ marginTop: 120, color: "#9d9d9d", fontSize: 18 }}>
            @2020 MOCA
          </div>
          <div style={{ marginTop: 32 }}>
            <img src="/scrollDown.png" width="20" height="60" />
          </div>
        </div>
        <div
          style={{
            paddingTop: 68,
            textAlign: "center",
            backgroundColor: "#EAE6D4",
            height: 1013,
          }}
        >
          <img src="/logo2.png" width="250" height="70" />
          <div
            style={{
              marginTop: 16,
              fontSize: 42,
              lineHeight: "40px",
              color: "#110000",
            }}
          >
            취향에 맞는 카페를 추천해 드립니다!
          </div>
          <div style={{ marginTop: 60 }}>
            <span>
              <img src="/01.png" width="381" height="721" />
            </span>
            <span style={{ marginLeft: 60 }}>
              <img src="/02.png" width="381" height="721" />
            </span>
            <span style={{ marginLeft: 60 }}>
              <img src="/03.png" width="381" height="721" />
            </span>
          </div>
        </div>
      </>
    );
  }
}
const mapStateToProps = (state) => ({
  resultYN: state.main.resultYN,
  message: state.main.message,
});

const mapDispatchToProps = (dispatch) => ({
  mailing: (params) => dispatch(actions.mailing(params)),
  mailingInit: () => dispatch(actions.mailingInit()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Home);
