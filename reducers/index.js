import { combineReducers } from "redux";
import main from "./main";
import user from "./user";
import cafe from "./cafe";

const rootReducer = combineReducers({
  main,
  // user,
  // cafe,
});

export default rootReducer;
