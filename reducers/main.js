import produce from "immer";

export const initialState = {
  resultYN: "N",
  message: null,
  isLoading: false,
};

export const MAILING_INIT = "MAILING_INIT";
export const MAILING_REQUEST = "MAILING_REQUEST";
export const MAILING_SUCCESS = "MAILING_SUCCESS";
export const MAILING_FAILURE = "MAILING_FAILURE";

export const LOAD_MAIN_PLACES_REQUEST = "LOAD_MAIN_PLACES_REQUEST";
export const LOAD_MAIN_PLACES_SUCCESS = "LOAD_MAIN_PLACES_SUCCESS";
export const LOAD_MAIN_PLACES_FAILURE = "LOAD_MAIN_PLACES_FAILURE";

export const BOOKMARK_PLACE_REQUEST = "BOOKMARK_PLACE_REQUEST";
export const BOOKMARK_PLACE_SUCCESS = "BOOKMARK_PLACE_SUCCESS";
export const BOOKMARK_PLACE_FAILURE = "BOOKMARK_PLACE_FAILURE";

export const UNBOOKMARK_PLACE_REQUEST = "UNBOOKMARK_PLACE_REQUEST";
export const UNBOOKMARK_PLACE_SUCCESS = "UNBOOKMARK_PLACE_SUCCESS";
export const UNBOOKMARK_PLACE_FAILURE = "UNBOOKMARK_PLACE_FAILURE";

export const CHANGE_RATE_PLACE_REQUEST = "CHANGE_RATE_PLACE_REQUEST";
export const CHANGE_RATE_PLACE_SUCCESS = "CHANGE_RATE_PLACE_SUCCESS";
export const CHANGE_RATE_PLACE_FAILURE = "CHANGE_RATE_PLACE_FAILURE";

const reducer = (state = initialState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case MAILING_INIT: {
        draft.isLoading = false;
        draft.resultYN = "N";
        draft.message = null;
        break;
      }
      case MAILING_REQUEST: {
        draft.isLoading = true;
        break;
      }
      case MAILING_SUCCESS: {
        draft.isLoading = false;
        draft.resultYN = "Y";
        draft.message = action.data.message;
        break;
      }
      case MAILING_FAILURE: {
        draft.isLoading = false;
        draft.resultYN = "N";
        draft.message = null;
        break;
      }
      case LOAD_MAIN_PLACES_REQUEST: {
        draft.hasMorePlace = true;
        break;
      }
      case LOAD_MAIN_PLACES_SUCCESS: {
        action.data.place.forEach((d) => {
          draft.mainPlaces.push(d);
        });
        draft.isLoading = false;
        draft.hasMorePost = action.data.length ? true : false;
        draft.activeTab = action.data.activeTab;
        break;
      }
      case LOAD_MAIN_PLACES_FAILURE: {
        draft.isLoading = false;
        break;
      }
      case BOOKMARK_PLACE_REQUEST: {
        break;
      }
      case BOOKMARK_PLACE_SUCCESS: {
        const placeIndex = draft.mainPlaces.findIndex(
          (v) => v.idx === action.data.placeIdx
        );
        draft.mainPlaces[placeIndex].bookmark = true;
        break;
      }
      case BOOKMARK_PLACE_FAILURE: {
        break;
      }
      case UNBOOKMARK_PLACE_REQUEST: {
        break;
      }
      case UNBOOKMARK_PLACE_SUCCESS: {
        const placeIndex = draft.mainPlaces.findIndex(
          (v) => v.idx === action.data.placeIdx
        );
        draft.mainPlaces[placeIndex].bookmark = false;
        break;
      }
      case UNBOOKMARK_PLACE_FAILURE: {
        break;
      }
      case CHANGE_RATE_PLACE_REQUEST: {
        break;
      }
      case CHANGE_RATE_PLACE_SUCCESS: {
        const placeIndex = draft.mainPlaces.findIndex(
          (v) => v.idx === action.data.placeIdx
        );
        draft.mainPlaces[placeIndex].rate = action.data.rate;
        break;
      }
      case CHANGE_RATE_PLACE_FAILURE: {
        break;
      }
      default: {
        break;
      }
    }
  });
};

export default reducer;
