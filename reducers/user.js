import produce from "immer";

export const initialState = {
  isLogin: false, // 로그인 여부
  isLogoutLoading: false, // 로그아웃 시도중
  isLoginLoading: false, // 로그인 시도중
  logInErrorReason: "", // 로그인 실패 사유
  isSignUp: false, // 회원가입 성공
  isSignUpLoading: false, // 회원가입 시도중
  signUpErrorReason: "", // 회원가입 실패 사유
  me: null, // 내 정보
  followingList: [], // 팔로잉 리스트
  followerList: [], // 팔로워 리스트
  userInfo: null, // 남의 정보
  errorState: [],
  errorCheck: false,
  errorMsg: "",
  smsErrorCheck: false,
  smsErrorMsg: "",
  mobileMsg: "",
  page: "",
  joinIdx: 0,
  chkIdx: 0,
};

export const LOAD_USER_REQUEST = "LOAD_USER_REQUEST";
export const LOAD_USER_SUCCESS = "LOAD_USER_SUCCESS";
export const LOAD_USER_FAILURE = "LOAD_USER_FAILURE";

export default (state = initialState, action) => {
  return produce(state, (draft) => {
    switch (action.type) {
      case LOAD_USER_REQUEST: {
        break;
      }
      case LOAD_USER_SUCCESS: {
        console.log("actionData:", action.data);
        if (action.data.state === "success") {
          draft.isLoginLoading = false;
          draft.isLogin = true;
          draft.me = action.data;
          draft.isLoading = false;
        }
        break;
      }
      case LOAD_USER_FAILURE: {
        break;
      }

      default: {
        break;
      }
    }
  });
};
