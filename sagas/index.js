import { all, call, fork } from "redux-saga/effects";
import axios from "axios";
import main from "./main";
import user from "./user";
import place from "./cafe";

axios.defaults.baseURL = "https://api.moca.so";
axios.defaults.headers = {
  "Content-Type": "application/json",
};

axios.interceptors.request.use(function (config) {
  if (typeof localStorage !== "undefined") {
    const accessToken = localStorage.getItem("accessToken");
    if (accessToken) {
      console.log("accesstoken:", accessToken);
      config.headers.Authorization = "Bearer " + accessToken;
    }
  }
  return config;
});

export default function* rootSaga() {
  yield all([fork(main)]);
}
