import {
  all,
  delay,
  fork,
  put,
  takeLatest,
  call,
  throttle,
} from "redux-saga/effects";
import axios from "axios";
import {
  MAILING_REQUEST,
  MAILING_SUCCESS,
  MAILING_FAILURE,
} from "../reducers/main";
import {
  LOAD_MAIN_PLACES_REQUEST,
  LOAD_MAIN_PLACES_SUCCESS,
  LOAD_MAIN_PLACES_FAILURE,
  BOOKMARK_PLACE_REQUEST,
  BOOKMARK_PLACE_SUCCESS,
  BOOKMARK_PLACE_FAILURE,
  UNBOOKMARK_PLACE_REQUEST,
  UNBOOKMARK_PLACE_SUCCESS,
  UNBOOKMARK_PLACE_FAILURE,
  CHANGE_RATE_PLACE_REQUEST,
  CHANGE_RATE_PLACE_SUCCESS,
  CHANGE_RATE_PLACE_FAILURE,
} from "../reducers/cafe";

function* watchMailing() {
  console.log("ddd");
  yield takeLatest(MAILING_REQUEST, loadMailing);
}

function* loadMailing(action) {
  try {
    const result = yield call(loadMailingAPI, action.data);
    yield put({
      type: MAILING_SUCCESS,
      data: {
        message: result.data.data.message,
      },
    });
  } catch (e) {
    console.log("error", e);
    yield put({
      type: MAILING_FAILURE,
      error: e,
    });
  }
}

function loadMailingAPI(data) {
  return axios.post("/home/mailing", data);
}

function* watchLoadMainPlaces() {
  yield throttle(1000, LOAD_MAIN_PLACES_REQUEST, loadMainPlaces);
}

function* loadMainPlaces(action) {
  try {
    const result = yield call(loadMainPlacesAPI, action.data);
    console.log("aaA:", result.data);
    yield put({
      type: LOAD_MAIN_PLACES_SUCCESS,
      data: {
        activeTab: action.data.activeTab,
        place: result.data,
      },
    });
  } catch (e) {
    console.log("error", e);
    yield put({
      type: LOAD_MAIN_PLACES_FAILURE,
      error: e,
    });
  }
}

function loadMainPlacesAPI(data) {
  return axios.post("/places", data);
}

function* watchBookmarkPlace() {
  yield takeLatest(BOOKMARK_PLACE_REQUEST, bookmarkPlace);
}

function* bookmarkPlace(action) {
  try {
    const result = yield call(bookmarkPlaceAPI, action.data);
    yield put({
      type: BOOKMARK_PLACE_SUCCESS,
      data: {
        placeIdx: action.data.placeIdx,
      },
    });
  } catch (e) {
    console.log("error", e);
    yield put({
      type: BOOKMARK_PLACE_FAILURE,
      error: e,
    });
  }
}

function bookmarkPlaceAPI(postData) {
  return axios.post("/place/bookmark", postData);
}

function* watchUnbookmarkPlace() {
  yield takeLatest(UNBOOKMARK_PLACE_REQUEST, unbookmarkPlace);
}

function* unbookmarkPlace(action) {
  try {
    const result = yield call(unbookmarkPlaceAPI, action.data);
    yield put({
      type: UNBOOKMARK_PLACE_SUCCESS,
      data: {
        placeIdx: action.data.placeIdx,
      },
    });
  } catch (e) {
    console.log("error", e);
    yield put({
      type: UNBOOKMARK_PLACE_FAILURE,
      error: e,
    });
  }
}

function unbookmarkPlaceAPI(postData) {
  return axios.post("/place/unbookmark", postData);
}

function* watchChangeRatePlace() {
  yield takeLatest(CHANGE_RATE_PLACE_REQUEST, changeRatePlace);
}

function* changeRatePlace(action) {
  try {
    const result = yield call(changeRatePlaceAPI, action.data);
    yield put({
      type: CHANGE_RATE_PLACE_SUCCESS,
      data: {
        placeIdx: action.data.placeIdx,
        rate: action.data.rate,
      },
    });
  } catch (e) {
    console.log("error", e);
    yield put({
      type: CHANGE_RATE_PLACE_FAILURE,
      error: e,
    });
  }
}

function changeRatePlaceAPI(postData) {
  return axios.post("/place/rate", postData);
}

export default function* mainSaga() {
  yield all([
    fork(watchMailing),
    fork(watchLoadMainPlaces),
    fork(watchBookmarkPlace),
    fork(watchUnbookmarkPlace),
    fork(watchChangeRatePlace),
  ]);
}
