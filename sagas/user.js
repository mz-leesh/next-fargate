import { all, fork, call, takeEvery, delay, put } from "redux-saga/effects";
import {
  LOAD_USER_REQUEST,
  LOAD_USER_SUCCESS,
  LOAD_USER_FAILURE,
} from "../reducers/user";
import axios from "axios";

// function loginAPI(postData) {
//   return axios.post("/user/login", postData);
// }
//
// function* login(action) {
//   try {
//     const result = yield call(loginAPI, action.data);
//     console.log("data:", result.data);
//     if (result.data.accessToken) {
//       if (typeof localStorage !== "undefined") {
//         //const accessToken = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.-Li6-6Q_T80-DbjMnQThwGl-xgemXEXK6sZ2gBbNAzo';
//         localStorage.setItem("accessToken", result.data.accessToken);
//       }
//     }
//     yield put({
//       type: LOG_IN_SUCCESS,
//       data: result.data,
//     });
//   } catch (e) {
//     console.log(e);
//     yield put({
//       type: LOG_IN_FAILURE,
//     });
//   }
// }
//
// function* watchLogin() {
//   yield takeEvery(LOG_IN_REQUEST, login);
// }
//
// function* logout() {
//   try {
//     if (typeof localStorage !== "undefined") {
//       const accessToken = localStorage.getItem("accessToken");
//       if (accessToken) {
//         localStorage.removeItem("accessToken");
//       }
//       yield put({
//         type: LOG_OUT_SUCCESS,
//       });
//     }
//   } catch (e) {
//     yield put({
//       type: LOG_OUT_FAILURE,
//     });
//   }
// }
//
// function* watchLogout() {
//   yield takeEvery(LOG_OUT_REQUEST, logout);
// }
//
// function followAPI(postData) {
//   return axios.post("/user/follow", postData);
// }
//
// function* follow(action) {
//   try {
//     const result = yield call(followAPI, action.data);
//     console.log(result.data);
//     yield put({
//       type: FOLLOW_USER_SUCCESS,
//       data: {
//         targetid: action.data.targetid,
//       },
//     });
//   } catch (e) {
//     console.error(e);
//     yield put({
//       type: FOLLOW_USER_FAILURE,
//       error: e,
//     });
//   }
// }
//
// function* watchUnfollow() {
//   yield takeEvery(UNFOLLOW_USER_REQUEST, unfollow);
// }
//
// function unfollowAPI(postData) {
//   return axios.post("/user/unfollow", postData);
// }
//
// function* unfollow(action) {
//   try {
//     const result = yield call(unfollowAPI, action.data);
//     console.log(result.data);
//     yield put({
//       type: UNFOLLOW_USER_SUCCESS,
//       data: {
//         targetid: action.data.targetid,
//       },
//     });
//   } catch (e) {
//     console.error(e);
//     yield put({
//       type: UNFOLLOW_USER_FAILURE,
//       error: e,
//     });
//   }
// }
//
// function* watchFollow() {
//   yield takeEvery(FOLLOW_USER_REQUEST, follow);
// }
//
// function* watchSignUp() {
//   yield takeEvery(SIGN_UP_REQUEST, signUp);
// }
//
// function* signUp(action) {
//   try {
//     const result = yield call(signUpAPI, action.data);
//     yield put({
//       type: SIGN_UP_SUCCESS,
//       data: result.data,
//     });
//   } catch (e) {
//     console.error(e);
//     yield put({
//       type: SIGN_UP_FAILURE,
//       error: e,
//     });
//   }
// }
//
// function signUpAPI(data) {
//   return axios.post("/users/join", data);
// }
//
// function* watchSignUpNext() {
//   yield takeEvery(SIGN_UP_NEXT_REQUEST, signUpNext);
// }
//
// function* signUpNext(action) {
//   try {
//     const result = yield call(signUpNextAPI, action.data);
//     yield put({
//       type: SIGN_UP_NEXT_SUCCESS,
//       data: result.data,
//     });
//   } catch (e) {
//     console.error(e);
//     yield put({
//       type: SIGN_UP_NEXT_FAILURE,
//       error: e,
//     });
//   }
// }
//
// function signUpNextAPI(data) {
//   return axios.post("/users/joinNext", data);
// }
//
function* watchLoadUser() {
  yield takeEvery(LOAD_USER_REQUEST, loadUser);
}

function* loadUser(action) {
  try {
    const result = yield call(loadUserAPI, action.data);
    yield put({
      type: LOAD_USER_SUCCESS,
      data: result.data,
    });
  } catch (e) {
    console.error(e);
    yield put({
      type: LOAD_USER_FAILURE,
      error: e,
    });
  }
}

function loadUserAPI(data) {
  return axios.post("/user/me", data);
}
//
// function* watchUserJoinCheck() {
//   yield takeEvery(USER_JOIN_CHECK_REQUEST, userJoinCheck);
// }
//
// function* userJoinCheck(action) {
//   try {
//     const result = yield call(userJoinCheckAPI, action.data);
//     console.log("chk:", result.data.chkIdx);
//     if (action.data.actionType === "joinChkSms") {
//       console.log("bbbbbbbb");
//       yield put({
//         type: SIGN_UP_NEXT_REQUEST,
//         data: {
//           joinIdx: action.data.joinIdx,
//           chkIdx: action.data.chkIdx,
//           smsAuthNum: action.data.smsAuthNum,
//           mobile: action.data.mobile,
//         },
//       });
//     } else {
//       console.log("aaaaaaa");
//       yield put({
//         type: USER_JOIN_CHECK_SUCCESS,
//         data: {
//           actionType: action.data.actionType,
//           state: result.data.state,
//           msg: result.data.msg,
//           chkIdx: result.data.chkIdx,
//         },
//       });
//     }
//   } catch (e) {
//     console.error(e);
//     yield put({
//       type: USER_JOIN_CHECK_FAILURE,
//       error: e,
//     });
//   }
// }
//
// function userJoinCheckAPI(data) {
//   return axios.post("/users/join-check", data);
// }
//
export default function* userSaga() {
  yield all([fork(watchLoadUser)]);
}
