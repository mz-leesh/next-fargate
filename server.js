const express = require("express");
const next = require("next");

const dev = process.env.NODE_ENV !== "production";
const app = next({ dev });
const handle = app.getRequestHandler();

app
  .prepare()
  .then(() => {
    const server = express();

    server.get("/board/:title", (req, res) => {
      const page = "/boardView";
      const params = { title: req.params.title };
      app.render(req, res, page, params);
    });

    server.get("*", (req, res) => {
      return handle(req, res);
    });

    server.listen(8080, (err) => {
      if (err) throw err;
      console.log("> Ready on Server Port: 8080");
    });
  })
  .catch((ex) => {
    console.error(ex.stack);
    process.exit(1);
  });
